/*------------------------------------------------------------------
 Graph.h
 Coming to the Rescue at Votuporanga

 Definition of the graph.

 Created by Neemias Freitas on 12/11/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#ifndef __Coming_to_the_Rescue_at_Votuporanga__Graph__
#define __Coming_to_the_Rescue_at_Votuporanga__Graph__
#include "Vertex.h"
#include <vector>


class Graph {

public:


	// Manager Methods (constructors & destructors)
	/*------------------------------------------------------------------
	 An optimized constructor for the graph.
	 It takes one argument (type), which represents the type of the graph to be created (directed d or undirected u).
	 ------------------------------------------------------------------*/
	Graph(std::string type);


	// Accessor Methods
	/*------------------------------------------------------------------
	 print displays all vertices in the graph with their adjacent vertices and costs.
	 ------------------------------------------------------------------*/
	void print(void);

	/*------------------------------------------------------------------
	 count returns the current size of the graph.
	 ------------------------------------------------------------------*/
	long count(void);


	// Mutator Methods
	/*------------------------------------------------------------------
	 addVertex inserts a new vertex pointer into the vertices vector.
	 The key value of this new vertex is given by the next available index of the vertices vector, which is given by its current size.
	 ------------------------------------------------------------------*/
	void addVertex(void);

	/*------------------------------------------------------------------
	 addEdge performs the addition of an edge for the graph.
	 It takes three parameters:
	 - a v1 vector: the initial vertex node of the edge;
	 - a v2 vector: the final vertex node of the edge;
	 - a cost: the cost of the connection between the initial and final vertex node.
	 ------------------------------------------------------------------*/
	void addEdge(unsigned v1, unsigned v2, unsigned cost);

	/*------------------------------------------------------------------
	 dijkstra performs the Dijkstra's shortest path algorithm.
	 It takes as paramater a start vertex and an end vertex to which the shortest path will be calculated.
	 ------------------------------------------------------------------*/
	void dijkstra(unsigned start, unsigned end);


private:

	// Object instance data
	std::vector<Vertex*> vertices;  // all vertices in the graph
	std::string type;  // type of graph (directed 'd' or undirected 'u')

	/*------------------------------------------------------------------
	 printPath displays the shortest path from a given start vertex to a given end vertex.
	 It is always called after the Dijkstra's algorithm has run.
	 ------------------------------------------------------------------*/
	void printPath(Vertex* vertex);

};


#endif /* defined(__Coming_to_the_Rescue_at_Votuporanga__Graph__) */