/*------------------------------------------------------------------
 Priority Queue.cpp
 Coming to the Rescue at Votuporanga

 Implementation of the priority queue (heap).

 Created by Neemias Freitas on 11/24/14. Modified by Neemias Freitas on 12/11/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Priority Queue.h"
using namespace std;


// Accessor Methods
/*------------------------------------------------------------------
 findMin returns the highest priority object from the priority queue.
 ------------------------------------------------------------------*/
Vertex* PQ::findMin(void) { return heap[0]; }

/*------------------------------------------------------------------
 count returns the number of vertices stored in the priority queue.
 ------------------------------------------------------------------*/
long PQ::count(void) { return heap.size(); }

/*------------------------------------------------------------------
 print displays the distance of the vertices in the binary heap using a level-order traversal.
 ------------------------------------------------------------------*/
void PQ::print(void) {
	for (auto& distance: heap)
		cout << distance << endl;
}

/*------------------------------------------------------------------
 findVertex searches in the priority queue for correspondent vertex with the same key value as given as parameter.
 ------------------------------------------------------------------*/
bool PQ::findVertex(unsigned key) {
	if (heap.size()) {
		for (auto& vertex: heap){
			if (vertex->getKey() == key)
			return true;
		}
	}
	return false;
}


// Mutator Methods
/*------------------------------------------------------------------
 insert inserts a new vertex pointer into the priority queue.
 Along with the insertion, a siftUp is performed.
 ------------------------------------------------------------------*/
void PQ::insert(Vertex* vertex) {
	heap.push_back(vertex);
	this->siftUp();
}

/*------------------------------------------------------------------
 siftUp slides up the last element of the priority queue to its correct position according to the heap-order property.
 ------------------------------------------------------------------*/
void PQ::siftUp(void) {
	long hole = heap.size() - 1;
	double distance = heap.back()->getDistance();
	for ( ; distance < heap[hole/2.01]->getDistance(); hole /= 2.01) {
		Vertex* temp = heap[hole/2.01];
		heap[hole/2.01] = heap[hole];
		heap[hole] = temp;
	}
}

/*------------------------------------------------------------------
 deleteMin removes and returns the highest priority object from the priority queue, which is located at the root.
 Along with the deletion, a siftDown is performed.
 ------------------------------------------------------------------*/
Vertex* PQ::deleteMin(void) {
	if (heap.size()) {
		Vertex* deleted = heap[0];
		heap[0] = heap.back();
		heap.pop_back();
		this->siftDown();
		return deleted;
	}
	return nullptr;
}

/*------------------------------------------------------------------
 siftDown slides down the first element of the priority queue to its correct position according to the heap-order property.
 ------------------------------------------------------------------*/
void PQ::siftDown(void) {
	double distance = heap[0]->getDistance();

	// When big enough, perform the following operation
	if (heap.size() >= 3) {
		unsigned hole = 0;
		unsigned child = 0;

		// Check if one of children's distance is smaller than the new distance
		for ( ; distance > min(heap[hole*2+1]->getDistance(), heap[hole*2+2]->getDistance()); hole = child) {  // two children
			Vertex* temp = ((heap[hole*2+1]->getDistance()) == min(heap[hole*2+1]->getDistance(), heap[hole*2+2]->getDistance())) ? heap[hole*2+1] : heap[hole*2+2];  // select the correct child vertex node
			child = ((heap[hole*2+1]->getDistance()) == min(heap[hole*2+1]->getDistance(), heap[hole*2+2]->getDistance())) ? hole*2+1 : hole*2+2;;  // select the correct child index
			heap[child] = heap[hole];
			heap[hole] = temp;

			// check if siftDown will go too far
			if (child*2+1 > heap.size()-1)  // no child
				break;

			else if (child*2+2 > heap.size()-1) {  // only one child
				if (distance > heap[child*2+1]->getDistance()) {
					temp = heap[child*2+1];
					heap[child*2+1] = heap[child];
					heap[child] = temp;
				}
				break;
			}
		}

	}

	// Otherwise, perform a simple check
	else if (heap.size() == 2) {
		if (distance > heap[1]->getDistance()) {
			Vertex* temp = heap[1];
			heap[1] = heap[0];
			heap[0] = temp;
		}
	}
}

/*------------------------------------------------------------------
 heapSort sorts the priority queue in decreasing order.
 ------------------------------------------------------------------*/
void PQ::heapSort(void) {
	vector<Vertex*> sortedQueue;
	while (heap.size()) { sortedQueue.push_back(this->deleteMin()); }
	heap.swap(sortedQueue);
}