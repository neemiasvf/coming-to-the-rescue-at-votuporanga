/*------------------------------------------------------------------
 Vertex.h
 Coming to the Rescue at Votuporanga
 
 Definition of the vertex node in a graph.

 Created by Neemias Freitas on 12/11/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#ifndef __Coming_to_the_Rescue_at_Votuporanga__Vertex__
#define __Coming_to_the_Rescue_at_Votuporanga__Vertex__
#include <iostream>
#include <map>


class Vertex {

public:


	// Manager Methods (constructors & destructors)
	/*------------------------------------------------------------------
	 An optimized constructor for the vertex node.
	 It takes one argument (key), which represents the next available index of the vector in which the vertex is being inserted.
	 ------------------------------------------------------------------*/
	Vertex(unsigned key);


	// Accessor Methods
	/*------------------------------------------------------------------
	 getKey returns the key value of the object.
	 ------------------------------------------------------------------*/
	unsigned getKey(void);

	/*------------------------------------------------------------------
	 getVisited returns the visited flag of the object.
	 ------------------------------------------------------------------*/
	bool getVisited(void);

	/*------------------------------------------------------------------
	 getDistance returns the distance value of the object.
	 ------------------------------------------------------------------*/
	double getDistance(void);

	/*------------------------------------------------------------------
	 getPath returns a pointer to the vertex object.
	 ------------------------------------------------------------------*/
	Vertex* getPath(void);

	/*------------------------------------------------------------------
	 getAdjList returns the map object containing the adjacent vertices of the object.
	 ------------------------------------------------------------------*/
	std::map<Vertex*, unsigned> getAdjList(void);


	// Mutator Methods
	/*------------------------------------------------------------------
	 setKey sets the key value of the object.
	 ------------------------------------------------------------------*/
	void setKey(unsigned key);

	/*------------------------------------------------------------------
	 setVisited sets the visited flag of the object.
	 ------------------------------------------------------------------*/
	void setVisited(bool visited);

	/*------------------------------------------------------------------
	 setDistance sets the distance value of the object.
	 ------------------------------------------------------------------*/
	void setDistance(double distance);

	/*------------------------------------------------------------------
	 setPath sets a pointer to the vertex object.
	 ------------------------------------------------------------------*/
	void setPath(Vertex* vertex);

	/*------------------------------------------------------------------
	 addNeighbor inserts a vertex pointer and its cost into the adjacent list of the object. This represents an edge.
	 ------------------------------------------------------------------*/
	void addNeighbor(Vertex* vertex, unsigned cost);


private:

	// Object instance data
	unsigned key;  // same value of its index in the vertices vector of the Graph class;
	bool visited;  // boolean flag indicating whether the vertex has been visited
	double distance;  // current distance from a given start node to this vertex
	Vertex* path;  // last origin vertex where the shortest path was found
	std::map<Vertex*, unsigned> adjList;  // list of adjacent vertices and their costs

};


#endif /* defined(__Coming_to_the_Rescue_at_Votuporanga__Vertex__) */