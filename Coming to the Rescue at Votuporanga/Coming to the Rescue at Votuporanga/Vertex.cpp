/*------------------------------------------------------------------
 Vertex.cpp
 Coming to the Rescue at Votuporanga
 
 Implementation of the vertex node in a graph.

 Created by Neemias Freitas on 12/11/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Vertex.h"
using namespace std;


// Manager Methods (constructors & destructors)
/*------------------------------------------------------------------
 An optimized constructor for the vertex node.
 It takes one argument (key), which represents the next available index of the vector in which the vertex is being inserted.
 Visited flag starts as false, distance as positive infinity and path as NULL.
 ------------------------------------------------------------------*/
Vertex::Vertex(unsigned key) {
	this->key = key;
	this->visited = false;
	this->distance = numeric_limits<double>::infinity();
	this->path = NULL;
}


// Accessor Methods
/*------------------------------------------------------------------
 getKey returns the key value of the object.
 ------------------------------------------------------------------*/
unsigned Vertex::getKey(void) { return this->key; }

/*------------------------------------------------------------------
 getVisited returns the visited flag of the object.
 ------------------------------------------------------------------*/
bool Vertex::getVisited(void) { return this->visited; }

/*------------------------------------------------------------------
 getDistance returns the distance value of the object.
 ------------------------------------------------------------------*/
double Vertex::getDistance(void) { return this->distance; }

/*------------------------------------------------------------------
 getPath returns a pointer to the vertex object.
 ------------------------------------------------------------------*/
Vertex* Vertex::getPath(void) { return this->path; }

/*------------------------------------------------------------------
 getAdjList returns the map object containing the adjacent vertices of the object.
 ------------------------------------------------------------------*/
map<Vertex*, unsigned> Vertex::getAdjList(void) { return this->adjList; }


// Mutator Methods
/*------------------------------------------------------------------
 setKey sets the key value of the object.
 ------------------------------------------------------------------*/
void Vertex::setKey(unsigned key) { this->key = key; }

/*------------------------------------------------------------------
 setVisited sets the visited flag of the object.
 ------------------------------------------------------------------*/
void Vertex::setVisited(bool visited) { this->visited = visited; }

/*------------------------------------------------------------------
 setDistance sets the distance value of the object.
 ------------------------------------------------------------------*/
void Vertex::setDistance(double distance) { this->distance = distance; }

/*------------------------------------------------------------------
 setPath sets a pointer to the vertex object.
 ------------------------------------------------------------------*/
void Vertex::setPath(Vertex* path) { this->path = path; }

/*------------------------------------------------------------------
 addNeighbor inserts a vertex pointer and its cost into the adjacent list of the object. This represents an edge.
 ------------------------------------------------------------------*/
void Vertex::addNeighbor(Vertex* vertex, unsigned cost) { this->adjList.emplace(vertex, cost); }