/*------------------------------------------------------------------
 Graph.cpp
 Coming to the Rescue at Votuporanga

 Implementation of the graph.

 Created by Neemias Freitas on 12/11/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include "Graph.h"
#include "Priority Queue.h"
using namespace std;


// Manager Methods (constructors & destructors)
/*------------------------------------------------------------------
 An optimized constructor for the graph.
 It takes one argument (type), which represents the type of the graph to be created (directed d or undirected u).
 ------------------------------------------------------------------*/
Graph::Graph(string type) { this->type = type; }


// Accessor Methods
/*------------------------------------------------------------------
 print displays all vertices in the graph with their adjacent vertices and costs.
 ------------------------------------------------------------------*/
void Graph::print(void) {

	for (auto& vertex: vertices) {
		cout << "Vector " << vertex->getKey() << endl;
		cout << "Adjacent - Cost:\n";

		for (auto& edge: vertex->getAdjList())
			cout << edge.first->getKey() << " - " << edge.second << endl << endl;
	}
}

/*------------------------------------------------------------------
 count returns the current size of the graph.
 ------------------------------------------------------------------*/
long Graph::count(void) { return this->vertices.size(); }

/*------------------------------------------------------------------
 printPath displays the shortest path from a given start vertex to a given end vertex.
 It is always called after the Dijkstra's algorithm has run.
 ------------------------------------------------------------------*/
void Graph::printPath(Vertex* vertex) {
	if (vertex->getPath()) {
		printPath(vertex->getPath());
		cout << "-";
	}
	cout << vertex->getKey();
}


// Mutator Methods
/*------------------------------------------------------------------
 addVertex inserts a new vertex pointer into the vertices vector.
 The key value of this new vertex is given by the next available index of the vertices vector, which is given by its current size.
 ------------------------------------------------------------------*/
void Graph::addVertex(void) { vertices.push_back(new Vertex(unsigned(vertices.size()))); }

/*------------------------------------------------------------------
 addEdge performs the addition of an edge for the graph.
 It takes three parameters:
 - a v1 vector: the initial vertex node of the edge;
 - a v2 vector: the final vertex node of the edge;
 - a cost: the cost of the connection between the initial and final vertex node.
 The addition is performed by setting each vertex adjacent to the other.
 If the graph is directed, only v2 will be adjacent to v1.
 If the graph is undirected, v2 will be adjacent to v1 and v1 will be adjacent to v2.
 ------------------------------------------------------------------*/
void Graph::addEdge(unsigned v1, unsigned v2, unsigned cost) {
	if (type == "u") {
		vertices[v1]->addNeighbor(vertices[v2], cost);
		vertices[v2]->addNeighbor(vertices[v1], cost);
	}
	else if (type == "d") {
		vertices[v1]->addNeighbor(vertices[v2], cost);
	}
}

/*------------------------------------------------------------------
 dijkstra performs the Dijkstra's shortest path algorithm.
 It takes as paramater a start vertex and an end vertex to which the shortest path will be calculated.
 ------------------------------------------------------------------*/
void Graph::dijkstra(unsigned start, unsigned end) {

	PQ unvisited; // binary heap for unvisited vertices

	// Reset distances and visited flags. 0 for start vertex and infinity for all others
	for (auto& vertex: vertices) {
		(vertex->getKey() == start) ? vertex->setDistance(0) : vertex->setDistance(numeric_limits<double>::infinity());
		vertex->setVisited(false);
	}

	unvisited.insert(vertices[start]);  // start with start vertex

	// While there is unvisited vertices...
	while (unvisited.count()) {

		Vertex* current = unvisited.deleteMin();  // get next unvisited vertex

		for (auto& adjacent: current->getAdjList()) {

			// If there is any adjacent vertices, i.e., edges
			if (!adjacent.first->getVisited()) {
				unsigned cost = adjacent.second;

				// If current path is shorter than before, update distance and path
				if (current->getDistance() + cost < adjacent.first->getDistance()) {
					adjacent.first->setDistance(current->getDistance() + cost);
					adjacent.first->setPath(current);

					// Add updated vertex to the unvisited heap
					if (!unvisited.findVertex(adjacent.first->getKey())) {
						unvisited.insert(adjacent.first);
					}
				}
			}
		}

		current->setVisited(true);

		// If the end vertex was visited, then algorithm is done
		if (vertices[end]->getVisited())
			break;
	}

	// Print shortest path
	if (vertices[end]->getDistance() == numeric_limits<double>::infinity()) {
		cout << "No existing connection (edge) between vertices...";
	}
	else {
		printPath(vertices[end]);
		cout << " cost of " << vertices[end]->getDistance();
	}
}