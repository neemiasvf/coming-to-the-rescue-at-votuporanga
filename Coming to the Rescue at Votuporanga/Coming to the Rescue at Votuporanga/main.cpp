/*------------------------------------------------------------------
 main.cpp
 Coming to the Rescue at Votuporanga

 A test driver for the graph class.

 Created by Neemias Freitas on 12/1/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#include <iostream>
#include <fstream>
#include <sstream>
#include "Graph.h"
using namespace std;


int main(int argc, const char * argv[]) {

	// Declare some useful variables
	string line, type;
	unsigned vertices_number;

	// Open input file
	ifstream input("cities.txt");

	// Read input file
	if (input.is_open()) {
		getline(input, line);
		istringstream iss(line);
		iss >> vertices_number;
		iss >> type;
		iss >> type;
	}

	// Instantiate graph
	Graph sao_paulo(type);

	// Add vertices to graph
	for (unsigned i = 0; i < vertices_number; i++) {
		sao_paulo.addVertex();
	}

	// Add edges to graph
	while (getline(input, line)) {
		istringstream iss(line);
		double v1, v2, cost;
		iss >> v1;
		iss >> v2;
		iss >> cost;
		sao_paulo.addEdge(v1, v2, cost);
	}

	// Execute Dijkstra's algorithm for every vertex
	for (unsigned i = 0; i < sao_paulo.count()-1; i++) {
		cout << i+1 << ": ";
		sao_paulo.dijkstra(0, i+1);
		cout << endl;
	}

	return 0;
}