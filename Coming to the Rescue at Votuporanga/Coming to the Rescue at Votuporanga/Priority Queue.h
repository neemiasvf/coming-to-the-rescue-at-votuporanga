/*------------------------------------------------------------------
 Priority Queue.h
 Coming to the Rescue at Votuporanga

 Definition of the priority queue (heap).
 In this priority queue, the elements with the highest priority are those which have the lowest value.
 The priority queue is defined by a vector called heap containing vertices nodes.
 **Priority queue optimized to graph implementation.

 Created by Neemias Freitas on 11/24/14. Modified by Neemias Freitas on 12/11/14.
 Copyright (c) 2014 Neemias Freitas. All rights reserved.
 ------------------------------------------------------------------*/


#ifndef __How_Fast_Can_You_Sort___Priority_Queue__
#define __How_Fast_Can_You_Sort___Priority_Queue__
#include <vector>
#include "Vertex.h"


class PQ {

public:


	// Accessor Methods
	/*------------------------------------------------------------------
	 findMin returns the highest priority object from the priority queue.
	 ------------------------------------------------------------------*/
	Vertex* findMin(void);

	/*------------------------------------------------------------------
	 count returns the number of vertices stored in the priority queue.
	 ------------------------------------------------------------------*/
	long count(void);

	/*------------------------------------------------------------------
	 print displays the distance of the vertices in the binary heap using a level-order traversal.
	 ------------------------------------------------------------------*/
	void print(void);

	/*------------------------------------------------------------------
	 findVertex searches in the priority queue for correspondent vertex with the same key value as given as parameter.
	 ------------------------------------------------------------------*/
	bool findVertex(unsigned key);


	// Mutator Methods
	/*------------------------------------------------------------------
	 insert inserts a new vertex pointer into the priority queue.
	 Along with the insertion, a siftUp is performed.
	 ------------------------------------------------------------------*/
	void insert(Vertex* vertex);

	/*------------------------------------------------------------------
	 deleteMin removes and returns the highest priority object from the priority queue, which is located at the root.
	 Along with the deletion, a siftDown is performed.
	 ------------------------------------------------------------------*/
	Vertex* deleteMin(void);

	/*------------------------------------------------------------------
	 heapSort sorts the priority queue in decreasing order.
	 ------------------------------------------------------------------*/
	void heapSort(void);


private:

	// Object instance data
	std::vector<Vertex*> heap;  // vector containing all unvisited vertices

	// Mutator Methods
	/*------------------------------------------------------------------
	 siftUp slides up the last element of the priority queue to its correct position according to the heap-order property.
	 ------------------------------------------------------------------*/
	void siftUp(void);

	/*------------------------------------------------------------------
	 siftDown slides down the first element of the priority queue to its correct position according to the heap-order property.
	 ------------------------------------------------------------------*/
	void siftDown(void);

};

#endif /* defined(__How_Fast_Can_You_Sort___Priority_Queue__) */